import {Link} from 'react-router-dom';

export default function Menu() {
    return (
        <div>
            <Link to="/home">Home</Link> |
            <Link to="/recipe-list">Recipe List</Link> |
            <Link to="/recipe-form">New Recipe</Link>
        </div>
    )
}