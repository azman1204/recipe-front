import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

export default function RecipeList() {
    let [recipes, setRecipes] = useState([]);

    useEffect(() => {
        // this is like constructor
        console.log('im in constructor...');
        // call API here
        getRecipe();
    }, []);

    function getRecipe() {
        let url = 'http://localhost:5000/recipes';
        fetch(url)
        .then(data => data.json())
        .then(json => setRecipes(json));
    }

    function doDelete(id) {
        //alert(id);
        fetch(`http://localhost:5000/recipes/${id}/publish`, {method: 'DELETE'})
        .then(data => data.json())
        .then(json => {
            console.log(json);
            // get all the data again
            getRecipe();
        });
    }

    function doSearch(event) {
        //alert(event.target.value);
        let name = event.target.value;
        if (name.length > 3) {
            let url = 'http://localhost:5000/recipes/1';
            let params = {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({name: name})
            }
            fetch(url, params)
            .then(data => data.json())
            .then(json => setRecipes(json));
        }
    }

    let msg = 'Hello World';
    return (
        <div>
            <div>{ msg } { 1 + 2}</div>
            <button className="btn btn-primary mb-1">New Recipe</button>
            <input type='text' className="form-control mb-2" onKeyUp={(event) => doSearch(event)} />
            <table className="table table-bordered table-primary table-striped-columns">
                <thead>
                    <tr className="">
                        <th>No</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Cook Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    { recipes.map((data, no) => (
                        <tr key={data.id}>
                            <td>{ no + 1 }</td>
                            <td>{ data.name }</td>
                            <td>{ data.description }</td>
                            <td>{ data.cook_time }</td>
                            <td>
                                <Link to={ '/recipe-edit/' + data.id } className="btn btn-success btn-sm">Edit</Link>
                                <button className="btn btn-danger btn-sm" onClick={() => doDelete(data.id)}>Delete</button>
                            </td>
                        </tr>
                    )) }
                    
                </tbody>
            </table>
        </div>
    );
}