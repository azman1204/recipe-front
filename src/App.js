import RecipeList from "./RecipeList";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import RecipeForm from "./RecipeForm";
import Home from "./Home";
import Menu from './Menu';
import RecipeEdit from "./RecipeEdit";

function App() {
  return (
      <Router>
        <Menu />
        <Routes>
          <Route path="/recipe-list" element={<RecipeList />}></Route>
          <Route path="/recipe-form" element={<RecipeForm />}></Route>
          <Route path="/home" element={<Home />}></Route>
          <Route path="/recipe-edit/:id" element={<RecipeEdit />}></Route>
        </Routes>
      </Router>
  );
}

export default App;
