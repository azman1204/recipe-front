import { useRef } from 'react';

export default function RecipeForm() {
    let name = useRef('');
    let description = useRef('');
    let cook_time = useRef('');

    function doSave() {
        let myname = name.current.value;
        let mydescription = description.current.value;
        let mycook_time = cook_time.current.value;
        let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjc5NTU1NDg2LCJqdGkiOiIzYThkZmFjZC1mMGI3LTRmMzItYmJmNC0xYjk5YmEzZWVlOWYiLCJ0eXBlIjoiYWNjZXNzIiwic3ViIjo3LCJuYmYiOjE2Nzk1NTU0ODYsImV4cCI6MTY3OTU1NjM4Nn0.hLbqEtCempPlNyOJvyvFsA_5ypmOe1JqvAN1eu62Xy8';
        let url = 'http://localhost:5000/recipes';
        let param = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            body: JSON.stringify({name: myname, description: mydescription, cook_time: mycook_time})
        };
        fetch(url, param)
        .then(data => data.json())
        .then(json => {
            if (json.id) {
                alert('Successfully Post a new recipe');
            } else {
                alert('Technical error');
            }
        });
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-2">Name</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={name} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">Description</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={description} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">Cook Time</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={cook_time} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-2"></div>
                <div className="col-md-6">
                    <input type="button" onClick={ doSave } className="btn btn-primary" value="Submit" />
                </div>
            </div>
        </div>
    )
}