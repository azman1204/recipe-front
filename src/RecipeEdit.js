import { useParams } from 'react-router-dom';
import { useEffect, useState, useRef } from 'react';

export default function RecipeEdit() {
    let params = useParams();
    let [recipe, setRecipe] = useState({});
    let name = useRef('');
    let description = useRef('');
    let cook_time = useRef('');

    useEffect(() => {
        fetch('http://localhost:5000/recipes/' + params.id)
        .then(data => data.json())
        .then(json => {
            console.log("from effect", json);
            setRecipe(json);
            name.current.value = json.name;
            description.current.value = json.description;
            cook_time.current.value = json.cook_time;
        });
    }, []);

    function doSave() {
        let myname = name.current.value;
        let mydescription = description.current.value;
        let mycook_time = cook_time.current.value;
        let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcmVzaCI6dHJ1ZSwiaWF0IjoxNjc5NjIxMzg0LCJqdGkiOiJhYmZkYjkzYy1kNWQwLTQ0YjEtYWNiNC05ZmY1MjYzYzJiOTYiLCJ0eXBlIjoiYWNjZXNzIiwic3ViIjo3LCJuYmYiOjE2Nzk2MjEzODQsImV4cCI6MTY3OTYyMjI4NH0.lPf4CY8sdukPBHf2t7Ws6ouKBbP9FQ7O40Bp7QE_ANI';
        let url = 'http://localhost:5000/recipes/'+params.id;
        let param = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token },
            body: JSON.stringify({name: myname, description: mydescription, cook_time: mycook_time})
        };
        fetch(url, param) 
        .then(data => data.json())
        .then(json => {
            console.log(json);
            if (json.id) {
                alert('Successfully update a recipe');
            } else {
                alert('Technical error');
            }
        });
    }
    
    return (
        <div>
            { recipe.name }
            <div className="row">
                <div className="col-md-2">Name</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={name} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">Description</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={description} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">Cook Time</div>
                <div className="col-md-6">
                    <input type="text" className="form-control" ref={cook_time}/>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2"></div>
                <div className="col-md-6">
                    <input type="button" onClick={ doSave } className="btn btn-primary" value="Submit" />
                </div>
            </div>
        </div>
    )
}